package ru.kondratenko.nlmk;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ListMaker listMaker = new ListMaker();
        List<Object> list = new ArrayList<>();
        //list.add(1);
        list.add(new Person("Ivan", "Ivanov"));
        list.add(new Person("Roman", "Ivanov", "ljhg@mail.ru"));
        //list.add("dfa");
        for (List<Description> lizt : listMaker.makeList(list)) {
            System.out.println(lizt.toString());
        }
    }
}
